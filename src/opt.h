/* 
 * File:   opt.h
 * Author: CHANEL
 *
 * Created on 12. août 2015, 16:46
 */

#ifndef OPT_H
#define	OPT_H

enum where_type {user, group};
enum key_type {group_key};

struct where_params {
    enum key_type key;
    char* value; //to be changed later for void* or union
};

struct options {
    char from;
    struct where_params *where;
};
extern struct options opts;

int manage_options(int argc, char* const argv[]);

#endif	/* OPT_H */

