
#include "user.h"
#include "opt.h"
#include <pwd.h>
#include <grp.h>
#include <stdio.h>


void print_user(const struct passwd *user) {
    //TODO: select only the requested keys/fields
    printf("%s:%s:%u:%u:%s:%s:%s\n", user->pw_name, user->pw_passwd, user->pw_uid, user->pw_gid, user->pw_gecos, user->pw_dir, user->pw_shell);
}

int get_user() {

    struct group *g;
    struct passwd *u;
    
    //Check if a selection of entries should be performed
    if(opts.where != NULL) { //Make a selection of entries
        switch(opts.where->key) {
            case group_key:

                //Get the group entry from the database
                g = getgrnam(opts.where->value);
                if(g == NULL) {
                    fprintf(stderr, "Group %s not found.\n", opts.where->value);
                    return 0;
                }

                ////
                // First display users having the requested group for primary group
                setpwent();
                while(u = getpwent()) {
                    //The requested user has the searched group as main group -> print it
                    if(u->pw_gid == g->gr_gid)
                        print_user(u);                
                }
                endpwent();    

                //Than display all users which belong to the group
                //Note: this implementation seems less efficient because groups could have been searched in the previous loop (i.e. primary group loop))
                //However the complexity benefit is small in the worst case (N*M vs N*(M+1)) while the advantages are:
                //  - users with primary groups are displayed first, others in second (possibility to distinguish them))
                //  - in some cases getpwnam is even faster than iterating the db with getpwent, leading to an even faster implementation
                //(TODO: in the case only user name is requested then it could be displayed directly without search the user database)
                for(int i = 0; g->gr_mem[i] != NULL; i++) {
                    u = getpwnam(g->gr_mem[i]);
                    if(u != NULL)
                        print_user(u);
                    else {
                        fprintf(stderr, "Warning: user %s mentionned in group %s, but user was not found in the database", g->gr_mem[i], g->gr_name);
                    }
                }
                break;
            default:
                printf("Where function not implemented yet\n");
                return 1;
        }
    }
    else { //no selection -> print all database
        setpwent();
        while(u = getpwent())
            print_user(u);                
        endpwent();                    
    }
    return 0;
}
