#include <stdio.h>
#include "opt.h"
#include "user.h"


int main(int argc, char *argv[]) {

    if(manage_options(argc, argv))
        return 1;

    //Process different databases accroding to options
    switch(opts.from) {
        case user:
            return get_user();
        default:
            printf("From function not implemented yet\n");
            return 1;
    }
    
}
