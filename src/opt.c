#include "opt.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

struct options opts;

void print_usage() {
    printf("Usage: nssquery [-s key] [-f database] [-w key=value]\n");
}

int from_params(const char *args) {

    if( (strcmp(args, "user") == 0) || (strcmp(args, "passwd") == 0) )
        opts.from = user;
    else if(strcmp(args, "group") == 0)
        opts.from = group;
    else {
        fprintf(stderr, "Error: %s : unknown database (from option)\n", args);
        print_usage();
        return 1;
    }
        
    return 0;
}

int where_params(const char *args) {

    //Tokenize where options which are separated by '.'
    
        //Tokenize by =  to separate value and option

    //So far simple version to test for "group" selection
    
    //Find the = sign to separate value and option
    char *equal= strchr(args,'=');
    if(equal == NULL) {
        fprintf(stderr, "Error: %s : cannot find = sign to separate key from value\n", args);
        print_usage();
        return 1;
    }
    *equal = '\0'; //AIE AIE AIE
            
    //Create a new option
    if( (opts.where = (struct where_params*) malloc(sizeof(struct where_params))) == NULL) {
        fprintf(stderr, "Error: out of memory\n", args);
        return 1;
    }
    
    //Assign key to new option
    if(strcmp(args, "group") == 0)
        opts.where->key = group_key;
    else {
        fprintf(stderr, "Error: %s : unknown key (where option)\n", args);
        print_usage();
        return 1;
    }

    //Assign value to new option
    opts.where->value = equal+1;
    
    return 0;
}

void print_options() {

    printf("From: ");
    switch(opts.from) {
            case user:
                printf("user\n");
                break;
            case group:
                printf("group\n");
                break;
            default:
                printf("not defined or wrong value\n");
    }
    
    printf("Where: ");
    if(opts.where != NULL)
        switch(opts.where->key) {
            case group_key:
                printf("group=%s\n", (char*) opts.where->value);
        }
    else
        printf("none%s\n");
}

int manage_options(int argc, char * const argv[]) {
    
    int c, err;

    //Define default options
    opts.from = user;
    opts.where = NULL;
    
    while ((c = getopt (argc, argv, ":hs:f:w:")) != -1) {
        switch (c) {
            case 'f':
                if(err = from_params(optarg))
                    return err;
                break;
            case 'w':
                if(err = where_params(optarg))
                    return err;
                break;
            case 'h':
                print_usage();
                exit(0); //to be changed and handled in main ?
            case '?': // Invalid option
                fprintf (stderr, "Unknown option -%c.\n", optopt);
                return 1;
            case ':':
                fprintf (stderr, "Option -%c requires an argument.\n", optopt);
                return 1;
            default:
                fprintf(stderr, "Error: parsing options failed for an unknown reason.\n", c);
                return 1;
        }
    }
    print_options();
    return 0;
}
