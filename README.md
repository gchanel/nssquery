nssquery \- query the NSS database for users and groups

nssquery [-skey] [-fdatabse] [-wkey=value]

nssquery is a program to query users and groups based on several criteria.  This program is very close to the gentent command. However nssquery goal is to improve the flexibility to select entries in the databases (for instance it is possible to select all members of a group).
nssquery is based on a (lose) SQL syntax relying on three main options: -s (select), -f (from) and -w (where).

