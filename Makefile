BUILDDIR = build
SRCDIR = src
DESTDIR = /usr/local/bin

MANFILE = man/nssquery.1
MANDESTDIR = /usr/local/share/man/man1

CC = gcc
CFLAGS = -g -std=gnu99 -c

OBJECTS = $(BUILDDIR)/nssquery.o $(BUILDDIR)/opt.o $(BUILDDIR)/user.o
EXECUTABLE = $(BUILDDIR)/nssquery


all: dir $(EXECUTABLE)
	
dir:
	mkdir -p $(BUILDDIR)

$(EXECUTABLE): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(EXECUTABLE)

$(BUILDDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $< -o $@
	

# !!! Be careful not to do that out of the project directory
clean:
	rm $(OBJECTS) $(BUILDDIR)/nssquery
	rmdir $(BUILDDIR)

install:
	install --owner=root --group=root $(BUILDDIR)/nssquery $(DESTDIR)
	install --owner=root --group=root $(MANFILE) $(MANDESTDIR)